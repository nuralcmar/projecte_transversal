<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Agenda de contactos</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/login.css" />	
		
	</head>
	<body>
   		<section>
   			<div class="centro sombra">
					<h1>Acceso a la agenda</h1>
					<div class="float_left">
						<div class="separador_mini"></div>
						<div class="margen">
							<img src="img/user.png" width="45">			
						</div>
						<div class="separador_mini"></div>
						<div class="margen">
							<img src="img/pass.png" width="46">		
						</div>
					</div>
					<div>
						<div class="separador_mini"></div>
						<input type="text" id="user" class="caja_texto"/>
						<div id="usuario" class="separador_corto"></div>
						<input type="password" id="pass" class="caja_texto"/>
						<div id="password" class="separador_corto"></div>
					</div>
					<div class="separador_corto"></div>
					<div>
						<a href="#" id="access"><div class="boton sombra_min">Acceso agenda</div></a>					
					</div>
				</div>
   		</section>
   		<script type="text/javascript">
			document.getElementById("access").addEventListener("click", myFunction);
			function myFunction() {
				var user=document.getElementById("user").value;
				var pass=document.getElementById("pass").value;
				if (user=="") {
					document.getElementById("usuario").className="oculto";
					document.getElementById("usuario").innerHTML = "Introduce un usuario";
					document.getElementById("user").focus();
					document.getElementById("password").className="separador_corto";
					document.getElementById("password").innerHTML = "";			
				}
				else if(pass==""){
					document.getElementById("password").className="oculto";
					document.getElementById("password").innerHTML = "Introduce una contraseña";
					document.getElementById("pass").focus();
					document.getElementById("usuario").className="separador_corto";
					document.getElementById("usuario").innerHTML = "";			
				}
				else {
					window.location="listado.php?accion=logear&user="+user;
				}
				
			}
		</script>
	</body>
</html>
