<?php
	session_start();
	if ($_GET["accion"]==logear) {
		if(!isset($_SESSION["agenda"])){
			$listado=array(array("id"=> 1, "name"=>"Jordi Casas", "fecha"=>"1970-11-22", "mail"=>"jordi@gmail.com"),
					   	   array("id"=> 2, "name"=>"Ana Mir", "fecha"=>"1990-12-09", "mail"=>"ana@gmail.com"),
			    	   	   array("id"=> 3, "name"=>"Carlos Pérez", "fecha"=>"1967-04-12", "mail"=>"carlos@gmail.com"),
						   	array("id"=> 4, "name"=>"Laura Sánchez", "fecha"=>"1985-01-22", "mail"=>"laura@gmail.com"),
					       	array("id"=> 5, "name"=>"Sarah Pérez", "fecha"=>"1997-10-09", "mail"=>"sarah@gmail.com"),
					       	array("id"=> 6, "name"=>"Luis Monzón", "fecha"=>"1967-09-23", "mail"=>"Luis@gmail.com"));
			$_SESSION["agenda"]=$listado;
		}
		else{
			$listado=$_SESSION["agenda"];
		}
		$_SESSION["user"]=$_GET["user"];
	}
	elseif($_GET["accion"]==volver) {
		$listado=$_SESSION["agenda"];		
	}
	else{
		$listado=$_SESSION["agenda"];
		$count=count($listado)+1;
		$linea=array("id"=> $count, "name"=>$_GET["user"], "fecha"=>$_GET["fecha"], "mail"=>$_GET["mail"]);
		array_push($listado, $linea);
		$_SESSION["agenda"]=$listado;
	}
	foreach ($listado as $key => $row) {
		$aux[$key] = $row['name'];
	}
	array_multisort($aux, SORT_ASC, $listado);
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Agenda de contactos</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/listado.css" />	
	</head>
	<body>
		<div class="separador_largo"></div>
   		<section class="sombra">
   			<div class="title_div">
   				<div class="separador_mini"></div>
   				<h1>Agenda de <?php echo $_SESSION["user"]; ?></h1>
   				<div class="separador_mini"></div>
   				<div class="in_line form_contact sombra_min">
   					<a href="./form.php"><img src="img/formulariodecontacto.png" width="200"></a>
   				</div>	
   				<div class="in_line tope sombra_min">
   					<a href="./index.php"><img class="bordes" src="img/exit.png" width="75"></a>
   				</div>	
   				<div class="separador_largo"></div>
   			</div>
   			<div class="cabezera_div">
   				<div class="linea">Nombre</div>
   				<div class="linea">Nacido</div>
   				<div class="linea">Correo</div>
   				<div class="linea">Acciones</div>
   			</div>
   			
   			<?php
   				for($i=0;$i<count($listado);$i++) {
   					if ($i!=count($listado)-1) {
   					
   			?>
   			<div class="lista_div">
   				<div id="<?php echo $listado[$i]['name']; ?>" class="linea_list"><?php echo $listado[$i]["name"]; ?></div>
   				<div id="<?php echo $listado[$i]['fecha']; ?>" class="linea_list"><?php echo $listado[$i]["fecha"]; ?></div>
   				<div id="<?php echo $listado[$i]['mail']; ?>" class="linea_list"><?php echo $listado[$i]["mail"]; ?></div>
   				<div class="linea_list">
						<a href="#" onclick="editar('<?php echo $listado[$i]['name']; ?>', '<?php echo $listado[$i]['fecha']; ?>'
						                            , '<?php echo $listado[$i]['mail']; ?>')">
							<img class="delete_edit left sombra_min borde_min" width="25" src="img/edit.png">
						</a>&nbsp;
						<a href="#" onclick="borrar('<?php echo $listado[$i]['name']; ?>', '<?php echo $listado[$i]['fecha']; ?>'
						                            , '<?php echo $listado[$i]['mail']; ?>')">
							<img class="delete_edit der sombra_min borde_min" width="25" src="img/delete.png">
						</a>
					</div>
   			</div>
   			<?php
   					}
   					else{
   			?>
   			<div class="lista_div">
   				<div id="<?php echo $listado[$i]['name']; ?>" class="linea_list"><?php echo $listado[$i]["name"]; ?></div>
   				<div id="<?php echo $listado[$i]['fecha']; ?>" class="linea_list"><?php echo $listado[$i]["fecha"]; ?></div>
   				<div id="<?php echo $listado[$i]['mail']; ?>" class="linea_list"><?php echo $listado[$i]["mail"]; ?></div>
   				<div class="linea_list cent">
   					<a href="#" onclick="editar('<?php echo $listado[$i]['name']; ?>', '<?php echo $listado[$i]['fecha']; ?>'
						                            , '<?php echo $listado[$i]['mail']; ?>')">
							<img class="delete_edit left sombra_min borde_min" width="25" src="img/edit.png">
						</a>&nbsp;
						<a href="#" onclick="borrar('<?php echo $listado[$i]['name']; ?>', '<?php echo $listado[$i]['fecha']; ?>'
						                            , '<?php echo $listado[$i]['mail']; ?>')">
							<img class="delete_edit der sombra_min borde_min" width="25" src="img/delete.png">
						</a>
					</div>
   			</div>
   		    <?php
   					}
   				}
   			?>
   			<div class="separador_corto"></div>
   			<div class="separador_corto"></div>
   			<div class="paginacion_div">
   			<div class="pri_ult">Primero</div>
   			<div class="pri_ult">&#60;&#60;</div>
   			<?php 
   				for ($i = 0; $i < 5; $i++) {
   			?>
   				<div class="pri_ult"><?php echo $i+1; ?></div>
   			<?php
   				}
   			?>
   			<div class="pri_ult">&#62;&#62;</div>
   			<div class="pri_ult">Ultimo</div>
   			</div>
   			<div class="separador_largo"></div>
   		</section>
   		<div class="separador_largo"></div>
   		<script type="text/javascript">
   			function editar(nombre, fecha, mail) {
   				document.getElementById(nombre).style.color="green";
   				document.getElementById(fecha).style.color="green";
   			   document.getElementById(mail).style.color="green";
   				setInterval(function(){ 
   												document.getElementById(nombre).style.color="black";
   												document.getElementById(fecha).style.color="black";
   												document.getElementById(mail).style.color="black";
   											 }, 6000);
   			}
   			function borrar(nombre, fecha, mail) {
   				document.getElementById(nombre).style.color="red";
   				document.getElementById(fecha).style.color="red";
   			   document.getElementById(mail).style.color="red";
   				setInterval(function(){ 
   												document.getElementById(nombre).style.color="black";
   												document.getElementById(fecha).style.color="black";
   												document.getElementById(mail).style.color="black";
   											 }, 6000);
   			}
   		</script>
	</body>
</html>