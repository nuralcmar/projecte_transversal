<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Agenda de contactos</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/form.css" />
	</head>
	<body class="todo">
   		<section>
   			<div class="centro sombra">
					<h1>Insertar contacto</h1>
					<div class="float_left">
						<div class="separador_mini"></div>
						<div class="margen">
							<img src="img/user.png" width="45">			
						</div>
						<div class="separador_corto_mini"></div>
						<div class="margen">
							<img src="img/mail.png" width="46">		
						</div>
						<div class="separador_corto_mini"></div>
						<div class="margen">
							<img src="img/edad.png" width="44">		
						</div>
						<div class="separador_mini"></div>
					</div>
					<div>
						<div class="separador_mini"></div>
						<input type="text" id="user" class="caja_texto"/>
						<div id="nombre" class="separador_corto"></div>
						<input type="text" id="mail" class="caja_texto"/>
						<div id="correo" class="separador_corto"></div>
						<input type="text" id="date" class="caja_texto"/>
						<div id="fecha" class="separador_corto"></div>
					</div>
					<div id="fecha" class="separador_corto"></div>
					<a href="#" id="insert"><div class="boton_form float_izq sombra_min">Insertar</div></a>
					<a href="listado.php?accion=volver"><div class="boton_form float_inh sombra_min">Lista</div></a>
					<a href="index.php"><div class="boton_form float_der sombra_min">Salir</div></a>
				</div>
   		</section>
   		<script type="text/javascript">
			document.getElementById("insert").addEventListener("click", myFunction);
			function myFunction() {
				var user=document.getElementById("user").value;
				var mail=document.getElementById("mail").value;
				var fecha=document.getElementById("date").value;
				if (user=="") {
					document.getElementById("nombre").className="oculto";
					document.getElementById("nombre").innerHTML = "Introduce un nombre";
					document.getElementById("fecha").className="separador_corto";
					document.getElementById("correo").className="separador_corto";
					document.getElementById("fecha").innerHTML = "";
					document.getElementById("correo").innerHTML = "";
				}
				else if(mail==""){
					document.getElementById("correo").className="oculto";
					document.getElementById("correo").innerHTML = "Introduce un mail";
					document.getElementById("fecha").className="separador_corto";
					document.getElementById("nombre").className="separador_corto";	
					document.getElementById("fecha").innerHTML = "";
					document.getElementById("nombre").innerHTML = "";		
				}
				else if(fecha==""){
					document.getElementById("fecha").className="oculto";
					document.getElementById("fecha").innerHTML = "Introduce una fecha";
					document.getElementById("nombre").className="separador_corto";
					document.getElementById("correo").className="separador_corto";			
					document.getElementById("nombre").innerHTML = "";
					document.getElementById("correo").innerHTML = "";
				}
				else {
					window.location="listado.php?accion=insertar&user="+user+"&mail="+mail+"&fecha="+fecha;
				}
				
			}
		</script>
	</body>
</html>
